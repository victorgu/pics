import axios from 'axios'

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization: 'Client-ID 8ba2a03aa8f2cef3b78f3e073a0a349fb4e73c30a780b3f297f16e1a6b155e3e ',
  }
})